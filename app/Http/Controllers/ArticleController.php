<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends Controller {
	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function articles() {
		return response()->json(Article::get(), 200);
	}

	public function articlesById($id) {
		return response()->json(Article::find($id), 200);
	}

	public function articlesByCategory($name) {
		return response()->json(DB::select("SELECT a.id, a.name, a.published, a.deleted, a.price FROM article a JOIN articlecategory b ON a.id = b.article_id JOIN category c ON b.category_id = c.id WHERE c.name LIKE '%$name%'"));
	}

	public function articlesByName($name) {
		return response()->json(Article::where("name", "like", "%$name%")->get());
	}

	public function articlesByPrice($price_from, $price_before) {
		return response()->json(Article::whereRaw("price between $price_from and $price_before")->get());
	}

	public function articlesPublished() {
		return response()->json(Article::where("published", "=", 1)->get());
	}

	public function articlesActual() {
		return response()->json(Article::where("deleted", "=", 0)->get());
	}

	/**
	 * @param Request $request
	 * @return ResponseFactory|Application|Response
	 */
	public function create(Request $request) {
		$article = Article::create($request->all());

		return response($article, 201);
	}

	/**
	 * @param Request $request
	 * @param Article $article
	 * @return ResponseFactory|Application|Response
	 */
	public function edit(Request $request, Article $article) {
		$article->update($request->all());

		return response($article, 200);
	}

	/**
	 * @param Request $request
	 * @param Article $article
	 * @return ResponseFactory|Application|Response
	 */
	public function delete(Request $request, Article $article) {
		$article->deleted = 1;
		$article->save();

		return response($article, 200);
	}
}


