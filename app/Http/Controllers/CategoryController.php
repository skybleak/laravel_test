<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\ArticleCategory;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller {
	/**
	 * @param Request $request
	 * @return ResponseFactory|Application|Response
	 */
	public function create(Request $request) {
		$category = Category::create($request->all());
		return response($category, 201);
	}

	/**
	 * @param Request $request
	 * @param Category $category
	 * @return \Illuminate\Http\JsonResponse
	 * @throws Exception
	 */
	public function delete(Request $request, Category $category) {
		try {
			ArticleCategory::where('category_id', $category->id)->firstOrFail();
		} catch (ModelNotFoundException $e) {
			if ($e instanceof ModelNotFoundException) {
				$category->delete();
			}
		}
		return response()->json(['error' => '1', 'message' => 'This category is used.']);
	}
}
